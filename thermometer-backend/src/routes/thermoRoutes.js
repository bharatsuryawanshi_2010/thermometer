const express = require('express')
const Thermo = require('../controller/thermo')
const upload = require('../config/multer-setup')
const route = express.Router()

route.post('/data',upload.single('file'),Thermo.fileUpload)

module.exports = route