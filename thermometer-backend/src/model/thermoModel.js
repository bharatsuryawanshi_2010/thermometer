const mongoose = require('mongoose')
const Schema = mongoose.Schema

const thermo = new Schema({
    tempData:{
        type:Array,
    }
})

const ThermoModel=mongoose.model('ThermoData',thermo)

module.exports = ThermoModel