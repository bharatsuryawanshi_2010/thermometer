const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const moment = require('moment')
const Thermo = require('../model/thermoModel')

let  getRandomColor =  function() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

const fileUpload = (req, res) => {

    let yearData, monthData=[], finalData=[], fileData=[], labels = [];
    let datasets = [{
        data: [],
        label: 'temperature',
        backgroundColor: []
    }]
    const jsonString = fs.readFileSync(req.file.path, "utf-8");
    const jsonObject = JSON.parse(jsonString)
    jsonObject.map(data => {
        let year = moment(data.ts).year()
        let month = moment(data.ts).month()
        let monthname = moment().month(parseInt(month)).format('MMMM')
        data.year = year
        data.month = monthname

        if(monthData.indexOf(monthname)==-1)
        monthData.push(monthname)
    })

    yearData = _.groupBy(jsonObject,'year')

    yearArray = Object.keys(yearData)
    
    if(monthData.length>0){

        monthData.map(month=>{
            let sum=0
            yearData[yearArray[0]].map(data=>{
                if(data.month==month)
                sum = sum + data.val
            })

            labels.push(month)
            datasets[0].data.push(sum)

            datasets[0].backgroundColor.push(getRandomColor().toString())

        })
        fileData.push({
            year:yearArray[0],
            data:{
                labels,
                datasets
            }
        })

        let thermo = new Thermo({
            tempData:fileData
        })

        thermo.save()
        .then(result=>{
            res.json({ success: true, result:result.tempData })
        })
        .catch(err=>{
            res.status(404).json({success:false,message:"Something went wrong",error_message:err})
        })

    }   
}

module.exports = { fileUpload: fileUpload }