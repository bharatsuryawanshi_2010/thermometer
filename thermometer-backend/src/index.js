const express = require('express')
const routes = require('./routes/thermoRoutes')
const bodyParser = require('body-parser')
const path = require('path')
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = 2100

const app = express()
const publicDir = path.join(__dirname, "./upload");
app.use(cors());
app.use(routes)
app.use(express.static(publicDir));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json({ limit: '200mb' }));
app.use(bodyParser.urlencoded({ limit: '200mb', extended: true, parameterLimit: 10000000 }));

app.listen(PORT,()=>{
    console.log(`Server is running on ${PORT}`);
    mongoose.connect('mongodb://localhost:27017/thermometer',{useNewUrlParser:true, useUnifiedTopology: true })
    .then(() => console.log("MongoDB Connection has been established successfully."))
    .catch(err => console.log(err)); 
})