import React from 'react';
import { Bar } from 'react-chartjs-2';

const Chart = ({responseData}) =>{
  if(responseData.result) console.log(responseData.result[0].data)
  return(
    <div>
    { responseData.result ? (
      <Bar
      data={responseData.result[0].data}
      width={100}
      height={20}
  /> 
)
: <div> Display chart </div>}
</div>
)
  
}

export default Chart;