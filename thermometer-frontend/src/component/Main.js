import React from 'react';
import Chart from './chart'

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      imageURL: '',
      responseData:{}
    };

    this.handleUploadImage = this.handleUploadImage.bind(this);
  }

  handleUploadImage(ev) {
    ev.preventDefault();

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);
    //data.append('filename', this.fileName.value);

    fetch('http://localhost:2100/data', {
      method: 'POST',
      body: data,
    }).then((response) => {
        console.log("response>>",response)
      response.json().then((body) => {
          console.log(JSON.stringify(body))
          this.setState({
            responseData: body
          })
      });
    });
  }

  render() {
    return (
      <form onSubmit={this.handleUploadImage}>
        <div>
          <input ref={(ref) => { this.uploadInput = ref; }} type="file" />
        </div>
        <br />
        <div>
          <button>Upload</button>
        </div>
        <Chart responseData={this.state.responseData}></Chart>
      </form>
    );
  }
}

export default Main;