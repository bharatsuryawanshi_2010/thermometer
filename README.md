# Project Title

Thermometer

## Getting Started

In this application we are going to visualize data of thermometer in the form of bar charts.

## Technology Stack

 1. **Node** For Back-end.
 2. **React JS** For Front-end.
 3. **MongoDB** For Database


## Prerequisites

1.  Node JS.
2.  MongoDB.

## How to run it.

### Node JS(Back-end)
1. Clone it on your local machine.
2. Go in your project directory and run command npm install.
3. For starting server run command npm start.
4. #### URL: http://localhost:2100/


### React JS(Front-end)
1. Clone it on your local machine.
2. Go in your project directory and run command npm install.
3. For starting server run command npm start.
4. #### URL: http://localhost:3000/


## How to use it.
1. Choose the file which contains the data for visualizing.
2. Wait for few seconds
3. Bar Chart will be shown according to data.